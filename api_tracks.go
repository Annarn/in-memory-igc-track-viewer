package main

import (
	"encoding/json"
	"fmt"
	"github.com/marni/goigc"
	"net/http"
	"strconv"
	"strings"
)

func replyWithTracks(w *http.ResponseWriter) {
	if len(db.tracks) == 0 {
		json.NewEncoder(*w).Encode([]Track{})

	} else {
		json.NewEncoder(*w).Encode(IDs)
	}
}

func replyWithTrack(w *http.ResponseWriter, db *TrackDB, id string) {
	intID, _ := strconv.Atoi(id) //String to int
	t, ok := db.Get(intID)
	if !ok {
		http.Error(*w, http.StatusText(404), http.StatusNotFound)
		return
	}
	json.NewEncoder(*w).Encode(t)

}

func replyWithField(w *http.ResponseWriter, db *TrackDB, id string, field string) {

	//is id valid
	intID, _ := strconv.Atoi(id) //String to int
	t, ok := db.Get(intID)
	if !ok {
		http.Error(*w, http.StatusText(404), http.StatusNotFound)
	}
	if field == "Track_length" {
		fmt.Fprint(*w, t.Track_length)
	} else if field == "H_Date" {
		fmt.Fprint(*w, t.H_Date)
	} else {
		returnedField, ok := db.getField(field, intID)
		if ok {
			fmt.Fprint(*w, returnedField)
		} else {
			http.Error(*w, http.StatusText(404), http.StatusNotFound)
			return
		}

	}

}

func handlerAPI(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	//Post request
	case "POST":
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) != 4 {
			http.Error(w, http.StatusText(400), http.StatusBadRequest)
			return
		}

		if parts[3] == "igc" {
			if r.Body == nil {
				http.Error(w, "Malformed URL", http.StatusBadRequest)
				return
			}
			//User specifies URL
			url := make(map[string]string)

			var t Track

			//Url decoded to a string
			err := json.NewDecoder(r.Body).Decode(&url)
			//Check for error or if suffix to string doesn't end with .igc
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			//Parsing igc file
			track, err := igc.ParseLocation(url["url"])
			//If URL is malformed or not point to an IGC file
			if err != nil {
				http.Error(w, "Problem reading the track", http.StatusBadRequest)
				return
			}
			//File to memory
			t.Pilot = track.Pilot
			t.Glider = track.GliderType
			t.Glider_id = track.GliderID
			t.H_Date = track.Header.Date
			t.Track_length = track.Points[0].Distance(track.Points[len(track.Points)-1])

			// new track
			id := db.Add(t)
			// add ID's to global int array
			IDs = append(IDs, id)
			//Respond with track ID
			json.NewEncoder(w).Encode(id)

			return

		} else {
			http.Error(w, http.StatusText(404), http.StatusBadRequest)
			return
		}
	//Get request
	case "GET":

		parts := strings.Split(r.URL.Path, "/")

		if len(parts) == 6 {
			http.Header.Add(w.Header(), "content-type", "text/plain")
		} else {
			http.Header.Add(w.Header(), "content-type", "application/json")
		}

		//General rules
		if parts[1] == "igcinfo" && parts[2] != "api" {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		} else if len(parts) == 5 && parts[3] != "igc" || len(parts) == 6 && parts[3] != "igc" {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		}

		//Decides what is shown according to url and makes sure the url is in correct format
		if  len(parts) == 3 && parts[2] == "api" {
			replyWithAPIinformation(&w)
		} else if len(parts) == 4 && parts[3] == "igc" {
			replyWithTracks(&w)
		} else if  len(parts) == 5 && parts[4] != ""{
			replyWithTrack(&w, &db, parts[4])
		} else if  len(parts) == 6 && parts[5] != "" {
			replyWithField(&w, &db, parts[4], parts[5])
		} else {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		}

	}
}